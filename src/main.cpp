#include "main.h"

MAX6675 ktc(ktcCLK, ktcCS, ktcSO);

void setup() {
  Serial.begin(9600);

  digitalWrite(ssrPin, LOW);
  pinMode(ssrPin, OUTPUT);

  // Initialize thermocouple reading time variable
  nextRead = millis();
  initialTime = 0;

  reflowState = IDLE;
  switchStatus = 1;
  input = 32;
  temp = 28;
  relay = 0;
}

void loop() {
  if (millis() > nextRead)
  {
    Serial.print(relay);
    if(relay == 1){
      Serial.print("on ");
      temp += 2;
    } else{
      Serial.print("off ");
      if(temp > 30){
        temp -= 1;
      }
    }
    // Read thermocouple next sampling period
    nextRead += SENSOR_SAMPLING_TIME;
    input = ktc.readCelsius();
    stateMachine();
    Serial.print("time: ");
    timeSeconds = millis()/1000;
    Serial.print(timeSeconds - initialTime);
    Serial.print(" - temp: ");
    Serial.print(input);
    Serial.print(" setpoint: ");
    Serial.print(setpoint);
    ssrControl();
    Serial.print("\n");
  }
}

void stateMachine()
{
  switch (reflowState){
    case IDLE:
    // If oven temperature is still above room temperature
    if (input >= TEMPERATURE_ROOM)
    {
      reflowState = TOO_HOT;
    }
    else
    {
      // If switch is pressed to start reflow process
      if (switchStatus == 1)
      {
        switchStatus = 0;
        Serial.println("Initializing reflow oven");

        initialTime = millis()/1000;
        // Ramp up to minimum soaking temperature
        setpoint = TEMPERATURE_SOAK_MIN + 10;
        // Tell the PID to range between 0 and the full window size
        pidMaxLim = WINDOW_SIZE;
        kp = PID_KP_PREHEAT;
        ki = PID_KI_PREHEAT;
        kd = PID_KD_PREHEAT;
        // Proceed to preheat stage
        reflowState = PREHEAT;
      }
    }
    break;

    case PREHEAT:
    Serial.println("preheat");
    reflowStatus = REFLOW_STATUS_ON;
    // If minimum soak temperature is achieve
    if (input >= TEMPERATURE_SOAK_MIN)
    {
      // Chop soaking period into smaller sub-period
      timerSoak = millis() + SOAK_MICRO_PERIOD;
      // Set less agressive PID parameters for soaking ramp
      kp = PID_KP_SOAK;
      ki = PID_KI_SOAK;
      kd = PID_KD_SOAK;
      // Ramp up to first section of soaking temperature
      setpoint = TEMPERATURE_SOAK_MIN + SOAK_TEMPERATURE_STEP + 10;
      // Proceed to soaking state
      reflowState = SOAK;
    }
    break;

    case SOAK:
    Serial.println("soak");
    // If micro soak temperature is achieved
    if (millis() > timerSoak)
    {
      timerSoak = millis() + SOAK_MICRO_PERIOD;
      // Increment micro setpoint
      setpoint += SOAK_TEMPERATURE_STEP;
      if (setpoint > TEMPERATURE_SOAK_MAX)
      {
        // Set agressive PID parameters for reflow ramp
        kp = PID_KP_REFLOW;
        ki = PID_KI_REFLOW;
        kd = PID_KD_REFLOW;
        // Ramp up to first section of soaking temperature
        setpoint = TEMPERATURE_REFLOW_MAX + 10;
        // Proceed to reflowing state
        reflowState = REFLOW;
      }
    }
    break;

    case REFLOW:
    Serial.println("reflow");
    // We need to avoid hovering at peak temperature for too long
    // Crude method that works like a charm and safe for the components
    if (input >= (TEMPERATURE_REFLOW_MAX - 5))
    {
      // Set PID parameters for cooling ramp
      kp = PID_KP_REFLOW;
      ki = PID_KI_REFLOW;
      kd = PID_KD_REFLOW;
      // Ramp down to minimum cooling temperature
      setpoint = TEMPERATURE_COOL_MIN - 20;
      // Proceed to cooling state
      reflowState = COOL;
    }
    break;

    case COOL:
    Serial.println("cool");
    // If minimum cool temperature is achieve
    if (input <= TEMPERATURE_COOL_MIN)
    {
      // Turn off reflow process
      reflowStatus = REFLOW_STATUS_OFF;
      // Proceed to reflow Completion state
      reflowState = COMPLETE;
    }
    break;

    case COMPLETE:
    Serial.println("Clomplete");
    reflowStatus = REFLOW_STATUS_OFF;
    // Reflow process ended
    reflowState = IDLE;
    break;

    case TOO_HOT:
    // If oven temperature drops below room temperature
    if (input < TEMPERATURE_ROOM)
    {
      Serial.println("Too hot");
      // Ready to reflow
      reflowState = IDLE;
    }
    break;
  }

}

void pid()
{
  error = setpoint - input;
  interror += (ki * error);
  if(interror > pidMaxLim)
    interror = pidMaxLim;
  if(interror < 0)
    interror = 0;
  diferror = error - prevError;

  prevError = error;

  output = (kp * error) + interror - (kd * diferror);

  if(output > pidMaxLim)
    output = pidMaxLim;
  if(output < 0)
    output = 0;
}

void ssrControl()
{
  if (reflowStatus == REFLOW_STATUS_ON)
  {
    now = millis();

    pid();

    Serial.print(" pid: ");
    Serial.print(output);
    Serial.print("\n");
    if(output >= 1000){
      digitalWrite(ssrPin, HIGH);
      relay = 1;
    }
    else{
      digitalWrite(ssrPin, LOW);
      relay = 0;
    }
  }
  else
  {
    digitalWrite(ssrPin, LOW);
    relay = 0;
  }
}
