#include <Arduino.h>
#include <max6675.h>

#ifndef MAIN_H
#define MAIN_H

typedef enum REFLOW_STATE
{
    IDLE,
    PREHEAT,
    SOAK,
    REFLOW,
    COOL,
    COMPLETE,
    TOO_HOT
} reflowState_t;

typedef enum REFLOW_STATUS
{
  REFLOW_STATUS_OFF,
  REFLOW_STATUS_ON
} reflowStatus_t;

#define TEMPERATURE_ROOM 50
#define TEMPERATURE_SOAK_MIN 150
#define TEMPERATURE_SOAK_MAX 200
#define TEMPERATURE_REFLOW_MAX 250
#define TEMPERATURE_COOL_MIN 95
#define SENSOR_SAMPLING_TIME 1000
#define SOAK_TEMPERATURE_STEP 5
#define SOAK_MICRO_PERIOD 9000
#define WINDOW_SIZE  2000;

// ***** PINS ASSIGNMENT *****
int ssrPin = 5; //D1
int ktcSO = 12; //D6
int ktcCS = 13; //D7
int ktcCLK = 14; //D5

// ***** PID PARAMETERS *****
// ***** PRE-HEAT STAGE *****
#define PID_KP_PREHEAT 100
#define PID_KI_PREHEAT 0.025
#define PID_KD_PREHEAT 20
// ***** SOAKING STAGE *****
#define PID_KP_SOAK 300
#define PID_KI_SOAK 0.05
#define PID_KD_SOAK 250
// ***** REFLOW STAGE *****
#define PID_KP_REFLOW 300
#define PID_KI_REFLOW 0.05
#define PID_KD_REFLOW 350
#define PID_SAMPLE_TIME 1000

// ***** PID CONTROL VARIABLES *****
double setpoint;
double input;
double output;
double error = 0;
double prevError = 0;
double interror = 0;
double diferror = 0;
double kp = PID_KP_PREHEAT;
double ki = PID_KI_PREHEAT;
double kd = PID_KD_PREHEAT;
unsigned long nextRead;
unsigned long timerSoak;
unsigned long buzzerPeriod;
int switchStatus;
int relay;
unsigned long now;
unsigned long pidMaxLim;
int timeSeconds;
int initialTime;

// Reflow oven controller state machine state variable
reflowState_t reflowState;
// Reflow oven controller status
reflowStatus_t reflowStatus;

int temp;

void stateMachine(void);
void pid(void);
void ssrControl(void);

#endif // MAIN_H
